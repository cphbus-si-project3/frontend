from flask import Flask, render_template, redirect, url_for, session, request, flash
from forms import survey
import requests
import json


app = Flask(__name__)
app.config['SECRET_KEY'] = 'nottoosecretkey'



@app.route('/')
def home():
    return render_template('home.html', title='Survey')

@app.route('/forms', methods=['POST','GET'])
def forms():
    forms = survey()
    datamodel = {}
    if request.method =='POST':
        location = request.form['location']
        gender = request.form['gender']
        age = request.form['age']
        rating = request.form['selected_rating']
        description = request.form['description']
        age = int(age)
        rating = int(rating)
       
        datamodel = {'location':location,'gender':gender, 'age':age, 'rating':rating,'description':description}
        jsonmodel = json.dumps(datamodel)    
        print(jsonmodel)       
        #a = requests.post('http://localhost:8080/Rating', headers = {'content-type': 'application/json'},
        #    data=jsonmodel)


        response = requests.post('http://backend/Rating', 
                        headers = {'content-type': 'application/json'},
                        data=jsonmodel)
        
     

        return render_template('success.html', location=location, gender=gender, age=age, rating=rating, description=description)
  
    return render_template('forms.html', forms=forms)

@app.route('/success', methods=['POST'])
def success():
  
    return render_template('success.html', title='success', forms = forms)



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
    #app.run(host='localhost', port=5000, debug=True)