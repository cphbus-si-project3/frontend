from flask_wtf import FlaskForm
from wtforms import Form, StringField, PasswordField, SubmitField, BooleanField, validators, DateField, SelectField, TextField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from datetime import datetime

class survey(FlaskForm):

    genderselect = [('1','male'),('2','female')]
    ratingselect = [('1','poor'),('2','inadequate'),('3','satisfactory'),('4','good'),('5','excellent')]

    location = StringField('location', validators=[DataRequired])
    gender = SelectField('gender', choices=genderselect, validators=[DataRequired])
    age = StringField('age',validators=[DataRequired])
    rating = SelectField('rating', choices=ratingselect, validators=[DataRequired])
    description = TextField('description')
    submit = SubmitField('Send feedback')

